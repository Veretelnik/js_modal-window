$.confirm = function (option){
return new Promise((resolve,reject)=>{
    const modal = $.modal({
        title: option.title,
        wigth:'400px',
        closable: false,
        content: option.content,
        onClose(){
            modal.destroy()
        },
        footerButtons: [
            {text:'Close', type:'secondary',handler(){
                modal.close()
                reject()
            }},
            {text:'Delete', type:'danger',handler(){
                modal.close()
                resolve()
            }}
        ]
    })
    setTimeout(()=> modal.open(),100)
})
}

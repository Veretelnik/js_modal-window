let  iphone = [
    {id:1,title:'Ipnone XS',price: '900',img:'https://stylus.ua/thumbs/568x568/f6/f7/839775.jpeg'},
    {id:2,title:'Ipnone Xr',price: '800',img:'https://stylus.ua/thumbs/390x390/7b/63/839821.jpeg'},
    {id:3,title:'Ipnone 11 Pro',price: '1000',img:'https://estore.ua/media/catalog/product/cache/8/image/650x650/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone_11_pro_max_mg_2_1_2.jpeg'}
]

const toHTML = iphone=>`
    <div class="col">
        <div class="card">
            <img class="card-img-top" style='height: 300px;' src="${iphone.img}" alt='${phone.title}'">
            <div class="card-body">
              <h5 class="card-title">Iphone ${iphone.title}</h5>
              <a href="#" class="btn btn-primary" data-btn='price' data-id='${iphone.id}'>Look the price</a>
              <a href="#" class="btn btn-danger" data-btn='remove' data-id='${iphone.id}'>Delete</a>
            </div>
          </div>
    </div>`

function render (){
    const html = iphone.map(toHTML).join('')
    document.querySelector('#phone').innerHTML=html
}
render()

const priceModal = $.modal({
    title: 'Цена на товар',
    closable: true,
    width: '400px',
    footerButtons:[
        {text:'Close', type:'primary',handler(){
            priceModal.close()
        }},
    ]
})


document.addEventListener('click',event=>{
    event.preventDefault()
    const btnType = event.target.dataset.btn
    const id = +event.target.dataset.id
    const phoneI = iphone.find(f=>f.id === id)
    if(btnType==='price'){
        priceModal.setContent(`
        <p>Цена на ${phoneI.title}: <strong>${phoneI.price}$<strong/><p/>
        `)
        priceModal.open()
    }else if (btnType === 'remove'){
        $.confirm({
            title: 'Ви впевнені?',
            content: `<p>Ви видаляєте телефон <strong>${phoneI.title}</strong></p>`
        }).then(()=>{
            iphone = iphone.filter(f=>f.id !==id)
            render()
        }).catch(()=>{
            console.log('Cancel');
            
        })
    }
})